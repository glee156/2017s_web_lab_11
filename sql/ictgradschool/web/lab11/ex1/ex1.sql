-- Answers to exercise 1 questions
SELECT dept
FROM unidb_courses;

-- Display semester and courses being attended
SELECT semester, dept, num
FROM unidb_attend;

-- List the student names and country, ordered by first name
SELECT fname, lname, country
FROM unidb_students
ORDER BY fname ASC;

-- List the student names and mentors, ordered by mentors
SELECT fname, lname, mentor
FROM unidb_students
ORDER BY mentor;

-- List the lecturers, ordered by office
SELECT fname, lname, office
FROM unidb_lecturers
ORDER BY office;

-- List the staff whose staff number is greater than 500
SELECT fname, lname, staff_no
FROM unidb_lecturers
WHERE staff_no > 500;

-- List the students whose id is greater than 1668 and less than 1824
SELECT fname, lname, id
FROM unidb_students
WHERE id > 1668 AND id < 1824;

-- List the students from NZ, Australia and US
SELECT fname, lname, country
FROM unidb_students
WHERE country in ("NZ", "AU", "US");

-- List the lecturers in G Block
SELECT fname, lname, office
FROM unidb_lecturers
WHERE office LIKE "G%";

-- List the courses not from the Computer Science Department
SELECT dept, num
FROM unidb_courses
WHERE NOT dept = "comp";

-- List the students from France or Mexico
SELECT fname, lname, country
FROM unidb_students
WHERE country in ("FR", "MX");