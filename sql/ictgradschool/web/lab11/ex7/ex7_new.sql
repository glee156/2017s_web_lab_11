DROP TABLE IF EXISTS has_player;
DROP TABLE IF EXISTS team;
DROP TABLE IF EXISTS player;


CREATE TABLE IF NOT EXISTS player(
  id int,
  name VARCHAR(50) NOT NULL,
  nationality VARCHAR(50),
  age int,
  PRIMARY KEY (id)
);

INSERT INTO player VALUES
  (4321, 'Henri', 'French', 35),
  (5432, 'Ahn', 'Korean', 28),
  (432, 'Zidane', 'French', 36),
  (6543, 'Park', 'Korean', 23),
  (9876, 'Lee', 'Korean', 24),
  (8765, 'Kim', 'Korean', 34),
  (321, 'Sarkozy', 'French', 25),
  (7654, 'Diane', 'French', 36);


CREATE TABLE IF NOT EXISTS team(
  name VARCHAR(50),
  home_city VARCHAR(50),
  point int,
  id int NOT NULL,
  captain int,
  FOREIGN KEY (captain) REFERENCES player(id),
  PRIMARY KEY (id)
);

INSERT INTO team VALUES
  ('Chelsea', 'Chelsea city', 200, 1234, 7654),
  ('Arsenal', 'Arsenal city', 300, 5678, 4321),
  ('Kia', 'Kia city', 400, 0123, 6543),
  ('Daewoo', 'Daewoo city', 100, 4567, 5432);


CREATE TABLE IF NOT EXISTS has_player(
  id int NOT NULL AUTO_INCREMENT,
  team int,
  player int,
  FOREIGN KEY (team) REFERENCES team(id),
  FOREIGN KEY (player) REFERENCES player(id),
  PRIMARY KEY (id)
);

INSERT INTO has_player(team, player) VALUES
  (5678, 4321),
  (4567, 5432),
  (0123, 6543),
  (1234, 7654),
  (0123, 8765),
  (4567, 9876),
  (0123, 321),
  (1234, 432);

SELECT *
FROM player p, team t, has_player h
  WHERE t.id = h.team
AND h.player = p.id;
