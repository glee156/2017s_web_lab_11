DROP TABLE IF EXISTS league;
DROP TABLE IF EXISTS team;
DROP TABLE IF EXISTS player;
DROP TABLE IF EXISTS has_team;
DROP TABLE IF EXISTS has_player;
DROP TABLE IF EXISTS assigns;

CREATE TABLE IF NOT EXISTS league (
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (name)
);

INSERT INTO league VALUE
  ('the mighty football league');


CREATE TABLE IF NOT EXISTS has_team(
  id int NOT NULL,
  league VARCHAR(50),
  team VARCHAR(50),
  PRIMARY KEY (id)
);

INSERT INTO has_team VALUES
  (1234, 'the mighty football league', 'Chelsea'),
  (5678, 'the mighty football league', 'Arsenal'),
  (0123, 'the mighty football league', 'Kia'),
  (4567, 'the mighty football leauge', 'Daewoo');

CREATE TABLE IF NOT EXISTS has_player(
  id int NOT NULL,
  team VARCHAR(50),
  player VARCHAR(50),
  PRIMARY KEY (id)
);

INSERT INTO has_player VALUES
  (4321, 'Arsenal', 'Henri'),
  (5432, 'Daewoo', 'Ahn'),
  (6543, 'Kia', 'Park'),
  (7654, 'Chelsea', 'Diane'),
  (8765, 'Kia', 'Kim'),
  (9876, 'Daewoo', 'Lee'),
  (321, 'Kia', 'Sarkozy'),
  (432, 'Chelsea', 'Zidane');

CREATE TABLE IF NOT EXISTS assigns(
  id int NOT NULL,
  player VARCHAR(50),
  PRIMARY KEY (id)
);

INSERT INTO assigns VALUES
  (1, 'Henri'),
  (2, 'Ahn'),
  (3, 'Zidane'),
  (4, 'Park'),
  (5, 'Lee'),
  (6, 'Kim'),
  (7, 'Sarkozy'),
  (8, 'Diane');

CREATE TABLE IF NOT EXISTS player(
  name VARCHAR(50) NOT NULL,
  nationality VARCHAR(50),
  age int,
  id int,
  league_id int,
  FOREIGN KEY (id) REFERENCES has_player(id),
  FOREIGN KEY (league_id) REFERENCES assigns(id),
  PRIMARY KEY (name)
);


INSERT INTO player VALUES
  ('Henri', 'French', 35, 4321, 1),
  ('Ahn', 'Korean', 28, 5432, 2),
  ('Zidane', 'French', 36, 432, 3),
  ('Park', 'Korean', 23, 6543, 4),
  ('Lee', 'Korean', 24, 9876, 5),
  ('Kim', 'Korean', 34, 8765, 6),
  ('Sarkozy', 'French', 25, 321, 7),
  ('Diane', 'French', 36, 7654, 8);


CREATE TABLE IF NOT EXISTS team(
  name VARCHAR(50) NOT NULL,
  home_city VARCHAR(50),
  point int,
  id int,
  captain VARCHAR(50),
  FOREIGN KEY (id) REFERENCES has_team(id),
  FOREIGN KEY (captain) REFERENCES player(name),
  PRIMARY KEY (name)
);

INSERT INTO team VALUES
  ('Chelsea', 'Chelsea city', 200, 1234, 'Diane'),
  ('Arsenal', 'Arsenal city', 300, 5678, 'Henri'),
  ('Kia', 'Kia city', 400, 0123, 'Park'),
  ('Daewoo', 'Daewoo city', 100, 4567, 'Ahn');
