-- Answers to exercise 2 questions

-- What are the names of the students who attend COMP219?
SELECT s.fname, s.lname
FROM unidb_students s, unidb_attend a
WHERE a.id = s.id
AND a.dept = "comp"
AND a.num = "219";

-- What are the names of the student reps that are not from NZ?
SELECT s.fname, s.lname
FROM unidb_students s, unidb_courses c
WHERE s.id = c.rep_id
AND country != "NZ";

-- Where are the offices for the lecturers of 219?
SELECT l.office
FROM unidb_lecturers l, unidb_teach t
WHERE l.staff_no = t.staff_no
AND t.num = 219;

-- What are the names of the students taught by Te Taka?
SELECT s.fname, s.lname
FROM unidb_students s, unidb_attend a, unidb_teach t, unidb_lecturers l
WHERE s.id = a.id
AND a.dept = t.dept
AND a.num = t.num
AND t.staff_no = l.staff_no
and l.fname = "Te Taka";

-- List the students and their mentors
SELECT s.fname, s.lname, t.fname, t.lname
FROM unidb_students s, unidb_students t
where s.mentor = t.id;

-- Name the lecturers whose office is in G-Block as well naming the students that are
-- not from NZ
SELECT l.fname, l.lname
FROM unidb_lecturers l
where l.office LIKE "G%"
UNION
  SELECT s.fname, s.lname
FROM unidb_students s
where s.country != "NZ";

-- List the course co-ordinator and student rep for COMP219
SELECT l.fname, l.lname
FROM unidb_lecturers l, unidb_courses c
WHERE l.staff_no = c.coord_no
AND c.num = 219
AND c.dept = 'comp'
UNION
  SELECT s.fname, s.lname
FROM unidb_students s, unidb_courses c
WHERE s.id = c.rep_id
AND c.num = 219
AND c.dept = 'comp';